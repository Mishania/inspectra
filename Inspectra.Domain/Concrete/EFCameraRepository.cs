﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inspectra.Domain.Abstract;
using Inspectra.Domain.Entities;


namespace Inspectra.Domain.Concrete
{
    public class EFCameraRepository : ICameraRepository
    {
        private EFDbContext context = new EFDbContext();
        public IQueryable<Camera> Cameras
        {
            get { return context.Cameras; }
        }


        public void SaveCamera(Camera camera)
        {
            if (camera.CameraID == 0)
            {
                context.Cameras.Add(camera);
            }
            else
            {
                Camera dbEntry = context.Cameras.Find(camera.CameraID);
                if (dbEntry != null)
                {
                    dbEntry.Name = camera.Name;
                    dbEntry.Description = camera.Description;
                    dbEntry.Location = camera.Location;
                    dbEntry.Address = camera.Address;
                    dbEntry.ImageData = camera.ImageData;
                    dbEntry.ImageMimeType = camera.ImageMimeType;
                }
            }
            context.SaveChanges();
        }


        public Camera DeleteCamera(int productID)
        {
            Camera dbEntry = context.Cameras.Find(productID);
            if (dbEntry != null)
            {
                context.Cameras.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

    }


}
