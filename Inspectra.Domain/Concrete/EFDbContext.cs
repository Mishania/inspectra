﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inspectra.Domain.Entities;
using System.Data.Entity;

namespace Inspectra.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet<Camera> Cameras { get; set; }
    }
}
