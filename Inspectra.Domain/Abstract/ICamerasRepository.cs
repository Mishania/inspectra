﻿using Inspectra.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspectra.Domain.Abstract
{
    public interface ICameraRepository
    {
        IQueryable<Camera> Cameras { get; }
        void SaveCamera(Camera camera);
        Camera DeleteCamera(int cameraID);
    }
}
