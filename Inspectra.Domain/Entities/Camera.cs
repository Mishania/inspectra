﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Inspectra.Domain.Entities
{
    public class Camera
    {
        //12312
        [HiddenInput(DisplayValue = false)]
        public int CameraID { get; set; }

        [Required(ErrorMessage = "Please enter a camera name")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please enter a description")]
        public string Description { get; set; }

        [Required (ErrorMessage = "Please enter location")]
        public string Location { get; set; }

        [Required(ErrorMessage = "Please specify a category")]
        public decimal Address  { get; set; }

        public byte[] ImageData { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType { get; set; }

    }
}
