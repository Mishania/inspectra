﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Moq;
using Inspectra.Domain.Abstract;
using Inspectra.Domain.Entities;
using System.Linq;
using Inspectra.Domain.Concrete;
using Inspectra.WebUI.Infrastructure.Abstract;
using Inspectra.WebUI.Infrastructure.Concrete;

namespace Inspectra.WebUI.Infrastructure
{
    // реализация пользовательской фабрики контроллеров, 
    // наследуясь от фабрики используемой по умолчанию
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;
        public NinjectControllerFactory()
        {
            // создание контейнера
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            // получение объекта контроллера из контейнера 
            // используя его тип
            return controllerType == null
              ? null
              : (IController)ninjectKernel.Get(controllerType);
        }
        private void AddBindings()
        {
            // конфигурирование контейнера

            //Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            //mock.Setup(m => m.Cameras).Returns(new System.Collections.Generic.List<Camera>{
            //    new Camera { Name = "Hik23", Address = 23 },
            //    new Camera { Name = "Axis21", Address = 24 },
            //    new Camera { Name = "Morbon22", Address = 25 }
            //}.AsQueryable());

            //ninjectKernel.Bind<ICameraRepository>().ToConstant(mock.Object);
            ninjectKernel.Bind<ICameraRepository>().To<EFCameraRepository>();

            ninjectKernel.Bind<IAuthProvider>().To<FormsAuthProvider>();
           
        }
    }
}