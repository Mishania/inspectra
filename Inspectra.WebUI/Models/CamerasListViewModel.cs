﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Inspectra.Domain.Entities;

namespace Inspectra.WebUI.Models
{
    public class CamerasListViewModel
    {
        public IEnumerable<Camera> Cameras { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentLocation { get; set; }
    }
}