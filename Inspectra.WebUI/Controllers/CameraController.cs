﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inspectra.Domain.Abstract;
using Inspectra.Domain.Entities;
using Inspectra.WebUI.Models;

namespace Inspectra.WebUI.Controllers
{
    public class CameraController : Controller
    {
        //
        // GET: /Camera/
        private ICameraRepository repository;
        public int PageSize = 2;
        public CameraController(ICameraRepository cameraRepository)
        {
            this.repository = cameraRepository;
        }

        //public ViewResult List()
        //{
        //    return View(repository.Cameras);
        //}


        public ViewResult List(string location, int page = 1)
        {
            //return View(repository.Cameras
            //  .OrderBy(p => p.CameraID)
            //  .Skip((page - 1) * PageSize)
            //  .Take(PageSize));

            CamerasListViewModel model = new CamerasListViewModel
            {
                Cameras = repository.Cameras
                .Where(p => location == null || p.Location == location)
                  .OrderBy(p => p.CameraID)
                  .Skip((page - 1) * PageSize)
                  .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = location == null ?
                        repository.Cameras.Count() :
                        repository.Cameras.Where(e => e.Location == location).Count()
                },

            CurrentLocation = location
            };

            return View(model);
        }

        public FileContentResult GetImage(int cameraId)
        {
            Camera prod = repository.Cameras.FirstOrDefault(p => p.CameraID == cameraId);
            if (prod != null)
            {
                return File(prod.ImageData, prod.ImageMimeType);
            }
            else
            {
                return null;
            }
        }

    }
}
