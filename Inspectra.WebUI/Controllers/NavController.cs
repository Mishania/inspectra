﻿using Inspectra.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inspectra.WebUI.Controllers
{
    public class NavController : Controller
    {
        //
        // GET: /Nav/

        private ICameraRepository repository;

        public NavController(ICameraRepository repo)
        {
            repository = repo;
        }

        public PartialViewResult Menu(string location = null)
        {
            ViewBag.SelectedLocation = location;
            IEnumerable<string> locations = repository.Cameras
              .Select(x => x.Location)
              .Distinct()
              .OrderBy(x => x);
            return PartialView(locations);

        }
    }
}
