﻿using Inspectra.Domain.Abstract;
using Inspectra.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inspectra.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        private ICameraRepository repository;

        public AdminController(ICameraRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index()
        {
            return View(repository.Cameras);
        }


        public ViewResult Edit(int cameraId)
        {
            Camera camera = repository.Cameras
              .FirstOrDefault(p => p.CameraID == cameraId);
            return View(camera);
        }


        //[HttpPost]
        //public ActionResult Edit(Camera camera)
        //{
        //    if (ModelState.IsValid)
        //    {          
        //        repository.SaveCamera(camera);
        //        TempData["message"] = string.Format("{0} has been saved", camera.Name);
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        // there is something wrong with the data values
        //        return View(camera);
        //    }
        //}

        [HttpPost]
        public ActionResult Edit(Camera camera, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    camera.ImageMimeType = image.ContentType;
                    camera.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(camera.ImageData, 0, image.ContentLength);
                }
                repository.SaveCamera(camera);
                TempData["message"] = string.Format("{0} has been saved", camera.Name);
                return RedirectToAction("Index");
            }
            else
            {
                // there is something wrong with the data values
                return View(camera);
            }
        }

        public ViewResult Create()
        {
            return View("Edit", new Camera());
        }


        [HttpPost]
        public ActionResult Delete(int cameraId)
        {
            Camera deletedCamera = repository.DeleteCamera(cameraId);
            if (deletedCamera != null)
            {
                TempData["message"] = string.Format("{0} was deleted", deletedCamera.Name);
            }
            return RedirectToAction("Index");
        }
    }
}
