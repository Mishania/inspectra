﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Inspectra.Domain.Entities;
using Inspectra.Domain.Abstract;
using Inspectra.WebUI.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web;


namespace Inspectra.UnitTests
{
    [TestClass]
    public class AdminTests
    {
        [TestMethod]
        public void Index_Contains_All_Products()
        {
            // Arrange - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
                new Camera {CameraID = 1, Name = "P1"},
                new Camera {CameraID = 2, Name = "P2"},
                new Camera {CameraID = 3, Name = "P3"},
              }.AsQueryable());
            // Arrange - create a controller
            AdminController target = new AdminController(mock.Object);
            // Action
            Camera[] result = ((IEnumerable<Camera>)target.Index().ViewData.Model).ToArray();
            // Assert
            Assert.AreEqual(result.Length, 3);
            Assert.AreEqual("P1", result[0].Name);
            Assert.AreEqual("P2", result[1].Name);
            Assert.AreEqual("P3", result[2].Name);
        }


        [TestMethod]
        public void Can_Edit_Product()
        {
            // Arrange - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
                new Camera {CameraID = 1, Name = "P1"},
                new Camera {CameraID = 2, Name = "P2"},
                new Camera {CameraID = 3, Name = "P3"},
              }.AsQueryable());
            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);
            // Act
            Camera p1 = target.Edit(1).ViewData.Model as Camera;
            Camera p2 = target.Edit(2).ViewData.Model as Camera;
            Camera p3 = target.Edit(3).ViewData.Model as Camera;
            // Assert
            Assert.AreEqual(1, p1.CameraID);
            Assert.AreEqual(2, p2.CameraID);
            Assert.AreEqual(3, p3.CameraID);
        }

        [TestMethod]
        public void Cannot_Edit_Nonexistent_Product()
        {
            // Arrange - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
                new Camera {CameraID = 1, Name = "P1"},
                new Camera {CameraID = 2, Name = "P2"},
                new Camera {CameraID = 3, Name = "P3"},
              }.AsQueryable());
            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);
            // Act
            Camera result = (Camera)target.Edit(4).ViewData.Model;
            // Assert
            Assert.IsNull(result);
        }


        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            // Arrange - create mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);
            // Arrange - create a product
            Camera camera = new Camera { Name = "Test" };
            // Act - try to save the product
            ActionResult result = target.Edit(camera, null);
            // Assert - check that the repository was called
            mock.Verify(m => m.SaveCamera(camera));
            // Assert - check the method result type
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            // Arrange - create mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);
            // Arrange - create a product
            Camera camera = new Camera { Name = "Test" };
            // Arrange - add an error to the model state
            target.ModelState.AddModelError("error", "error");
            // Act - try to save the product
            ActionResult result = target.Edit(camera, null);
            // Assert - check that the repository was not called
            mock.Verify(m => m.SaveCamera(It.IsAny<Camera>()), Times.Never());
            // Assert - check the method result type
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


        [TestMethod]
        public void Can_Delete_Valid_Products()
        {
            // Arrange - create a Product
            Camera cam = new Camera { CameraID = 2, Name = "Test" };
            // Arrange - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
                new Camera {CameraID = 1, Name = "P1"},
                cam,
                new Camera {CameraID = 3, Name = "P3"},
              }.AsQueryable());
            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);
            // Act - delete the product
            target.Delete(cam.CameraID);
            // Assert - ensure that the repository delete method was
            // called with the correct Product
            mock.Verify(m => m.DeleteCamera(cam.CameraID));
        }

    }
}
