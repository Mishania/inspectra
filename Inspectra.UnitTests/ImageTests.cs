﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inspectra.Domain.Entities;
using Moq;
using Inspectra.Domain.Abstract;
using System.Web.Mvc;
using Inspectra.WebUI.Controllers;
using System.Linq;

namespace Inspectra.UnitTests
{
    [TestClass]
    public class ImageTests
    {
        [TestMethod]
        public void Can_Retrieve_Image_Data()
        {
            // Arrange - create a Product with image data
            Camera prod = new Camera
            {
                CameraID = 2,
                Name = "Test",
                ImageData = new byte[] { },
                ImageMimeType = "image/png"
            };
            // Arrange - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
          new Camera {CameraID = 1, Name = "P1"},
          prod,
          new Camera {CameraID = 3, Name = "P3"}
        }.AsQueryable());
            // Arrange - create the controller
            CameraController target = new CameraController(mock.Object);
            // Act - call the GetImage action method
            ActionResult result = target.GetImage(2);    
            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileResult));
            Assert.AreEqual(prod.ImageMimeType, ((FileResult)result).ContentType);
        }


        [TestMethod]
        public void Cannot_Retrieve_Image_Data_For_Invalid_ID()
        {
            // Arrange - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
          new Camera {CameraID = 1, Name = "P1"},
          new Camera {CameraID = 2, Name = "P2"}
        }.AsQueryable());
            // Arrange - create the controller
            CameraController target = new CameraController(mock.Object);
            // Act - call the GetImage action method
            ActionResult result = target.GetImage(100);
            // Assert
            Assert.IsNull(result);
        }
    }
}
