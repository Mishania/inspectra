﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Inspectra.Domain.Abstract;
using Inspectra.Domain.Entities;
using Inspectra.WebUI.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Inspectra.WebUI.Models;
using Inspectra.WebUI.HtmlHelpers;

namespace Inspectra.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Paginate()
        {
            // Arrange
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
            new Camera {CameraID = 1, Name = "P1"},
            new Camera {CameraID = 2, Name = "P2"},
            new Camera {CameraID = 3, Name = "P3"},
            new Camera {CameraID = 4, Name = "P4"},
            new Camera {CameraID = 5, Name = "P5"}
          }.AsQueryable());

            CameraController controller = new CameraController(mock.Object);

            controller.PageSize = 3;

            // Act
            CamerasListViewModel result = (CamerasListViewModel)controller.List(null, 2).Model;

            // Assert
            Camera[] cameraArray = result.Cameras.ToArray();
            Assert.IsTrue(cameraArray.Length == 2);
            Assert.AreEqual(cameraArray[0].Name, "P4");
            Assert.AreEqual(cameraArray[1].Name, "P5");
        }


        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            // Arrange - define an HTML helper - we need to do this
            // in order to apply the extension method
            HtmlHelper myHelper = null;

            // Arrange - create PagingInfo data
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // Arrange - set up the delegate using a lambda expression
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Act
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            // Assert
            Assert.AreEqual(result.ToString(), @"<a href=""Page1"">1</a>"
              + @"<a class=""selected"" href=""Page2"">2</a>"
              + @"<a href=""Page3"">3</a>");
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            // Arrange
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
            new Camera {CameraID = 1, Name = "P1"},
            new Camera {CameraID = 2, Name = "P2"},
            new Camera {CameraID = 3, Name = "P3"},
            new Camera {CameraID = 4, Name = "P4"},
            new Camera {CameraID = 5, Name = "P5"}
            }.AsQueryable());

            // Arrange
            CameraController controller = new CameraController(mock.Object);
            controller.PageSize = 3;

            // Act
            CamerasListViewModel result = (CamerasListViewModel)controller.List(null, 2).Model;

            // Assert
            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);
        }

        [TestMethod]
        public void Can_Filter_Products()
        {
            // Arrange
            // - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
            new Camera {CameraID = 1, Name = "P1", Location = "Cat1"},
            new Camera {CameraID = 2, Name = "P2", Location = "Cat2"},
            new Camera {CameraID = 3, Name = "P3", Location = "Cat1"},
            new Camera {CameraID = 4, Name = "P4", Location = "Cat2"},
            new Camera {CameraID = 5, Name = "P5", Location = "Cat3"}
            }.AsQueryable());

            // Arrange - create a controller and make the page size 3 items
            CameraController controller = new CameraController(mock.Object);
            controller.PageSize = 3;

            // Action
            Camera[] result = ((CamerasListViewModel)controller.List("Cat2", 1).Model)
              .Cameras.ToArray();

            // Assert
            Assert.AreEqual(result.Length, 2);
            Assert.IsTrue(result[0].Name == "P2" && result[0].Location == "Cat2");
            Assert.IsTrue(result[1].Name == "P4" && result[1].Location == "Cat2");
        }


        [TestMethod]
        public void Can_Create_Categories()
        {
            // Arrange
            // - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
                new Camera {CameraID = 1, Name = "P1", Location = "Apples"},
                new Camera {CameraID = 2, Name = "P2", Location = "Apples"},
                new Camera {CameraID = 3, Name = "P3", Location = "Plums"},
                new Camera {CameraID = 4, Name = "P4", Location = "Oranges"},
                }.AsQueryable());

            // Arrange - create the controller
            NavController target = new NavController(mock.Object);

            // Act = get the set of categories
            string[] results = ((IEnumerable<string>)target.Menu().Model).ToArray();

            // Assert
            Assert.AreEqual(results.Length, 3);
            Assert.AreEqual(results[0], "Apples");
            Assert.AreEqual(results[1], "Oranges");
            Assert.AreEqual(results[2], "Plums");
        }


        [TestMethod]
        public void Indicates_Selected_Category()
        {
            // Arrange
            // - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
                mock.Setup(m => m.Cameras).Returns(new Camera[] {
                new Camera {CameraID = 1, Name = "P1", Location = "Apples"},
                new Camera {CameraID = 4, Name = "P2", Location = "Oranges"},
              }.AsQueryable());

            // Arrange - create the controller
            NavController target = new NavController(mock.Object);

            // Arrange - define the category to selected
            string locationToSelect = "Apples";

            // Action
            string result = target.Menu(locationToSelect).ViewBag.SelectedLocation;

            // Assert
            Assert.AreEqual(locationToSelect, result);
        }


        [TestMethod]
        public void Generate_Category_Specific_Product_Count()
        {
            // Arrange
            // - create the mock repository
            Mock<ICameraRepository> mock = new Mock<ICameraRepository>();
            mock.Setup(m => m.Cameras).Returns(new Camera[] {
            new Camera {CameraID = 1, Name = "P1", Location = "Cat1"},
            new Camera {CameraID = 2, Name = "P2", Location = "Cat2"},
            new Camera {CameraID = 3, Name = "P3", Location = "Cat1"},
            new Camera {CameraID = 4, Name = "P4", Location = "Cat2"},
            new Camera {CameraID = 5, Name = "P5", Location = "Cat3"}
          }.AsQueryable());

            // Arrange - create a controller and make the page size 3 items
            CameraController target = new CameraController(mock.Object);
            target.PageSize = 3;

            // Action - test the product counts for different categories
            int res1 = ((CamerasListViewModel)target
              .List("Cat1").Model).PagingInfo.TotalItems;
            int res2 = ((CamerasListViewModel)target
              .List("Cat2").Model).PagingInfo.TotalItems;
            int res3 = ((CamerasListViewModel)target
              .List("Cat3").Model).PagingInfo.TotalItems;
            int resAll = ((CamerasListViewModel)target
              .List(null).Model).PagingInfo.TotalItems;

            // Assert
            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);
        }

    }
}
